def draw_horiz_line(a):
    print(' --- ' * a)


def draw_vert_line(a):
    print('|    ' * (a+1))

if __name__ == "__main__":
    size = int(input('Please decide the size of your board:'))
    i = 0
    while i < size:
        draw_horiz_line(size)
        draw_vert_line(size)
        i += 1
draw_horiz_line(size)
