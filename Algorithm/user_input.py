# Waiting to be finished
board = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]


def put_xo(a, b, x):
    if 0 < a < 4 and 0 < b < 4:
        if board[a-1][b-1] == 0:
            board[a-1][b-1] = x
        else:
            print('This spot is already token. Please choose another spot')
    else:
        print('Please drop your piece inside the board.')


if __name__ == '__main__':
    user = 1
    for i in range(8):
        a = int(input('Please input the X coordinate:'))
        b = int(input('Please input the Y coordinate:'))
        if user == 1:
            put_xo(a, b, 1)
            user = 2
        else:
            put_xo(a, b, 2)
            user = 1
