import Algorithm.drawGameBoard_while

if __name__ == '__main__':
    size = int(input('Please decide the size of your board:'))
    for i in range(size):
        Algorithm.drawGameBoard_while.draw_horiz_line(size)
        Algorithm.drawGameBoard_while.draw_vert_line(size)
    Algorithm.drawGameBoard_while.draw_horiz_line(size)
