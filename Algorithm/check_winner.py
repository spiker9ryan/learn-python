def check_vertical(board, size_of_board):
    there_is_winner = False
    i, m = 0, 0
    print(size_of_board)
    print(there_is_winner is False)
    while i < size_of_board and there_is_winner is False:
        while m < size_of_board - 1:
            if board[m][i] != board[m+1][i] or board[i][m] == 0:
                break
            m += 1
        if m == size_of_board - 1:
            there_is_winner = True
            break
        else:
            i += 1
    if there_is_winner is True:
        return board[m][i]
    else:
        return False


def check_horizon(board, size_of_board):
    there_is_winner = False
    i, m = 0, 0
    while i < size_of_board and there_is_winner is False:
        while m < size_of_board - 1:
            if board[i][m] != board[i][m + 1] or board[i][m] == 0:
                break
            m += 1
        if m == size_of_board - 1:
            there_is_winner = True
            break
        else:
            i += 1
    if there_is_winner is True:
        return board[i][m]
    else:
        return False


def check_diagonal(board, size_of_board):
    i, m = 0, 0
    while i < size_of_board - 1:
        if board[i][m] != board[i+1][m+1] or board[i][m] == 0:
            break
        i += 1
        m += 1
    if i == size_of_board - 1:
        return board[i][m]
    else:
        i = size_of_board - 1
        m = 0
        while m < size_of_board - 1:
            if board[m][i] != board[m+1][i-1]:
                break
            m += 1
            i -= 1
        if m == size_of_board - 1:
            return board[m][i]


def check_winner(board):
    size_of_board = len(board)
    result = check_vertical(board, size_of_board)
    if result is not False:
        print('Winner is' + str(result))
    else:
        result = check_horizon(board, size_of_board)
        if result is not False:
            print('Winner is' + str(result))
        else:
            result = check_diagonal(board, size_of_board)
            if result is not False:
                print('Winner is' + str(result))
            else:
                print('There is no winner')


if __name__ == '__main__':
    check_winner([[1, 2, 1], [2, 1, 0], [1, 2, 0]])
