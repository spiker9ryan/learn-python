from collections import deque


if __name__ == '__main__':
    a = [3, 4, 5, 8, 2, 1, -1]
    list.sort(a)
    print(a)
    a.pop()
    print(a)

    stack = a
    stack.append(9)
    stack.append(6)
    print(stack)
    stack.pop()
    print(stack)

    queue = deque(a)
    print(queue)
    queue.append(10)
    queue.popleft()
    print(queue)
    print(a)